# Compile stage
FROM golang:latest AS build-env
ENV CGO_ENABLED 0
RUN apt-get install git
ADD . /src
WORKDIR /src

# Download dependencies using gomod
RUN go mod download

# The -gcflags "all=-N -l" flag helps us get a better debug experience
RUN go build -gcflags "all=-N -l" -o /app

# Compile Delve
RUN go get github.com/go-delve/delve/cmd/dlv

# Final stage
FROM alpine:latest AS export

# Port 8080 belongs to our application, 40000 belongs to Delve
EXPOSE 8080 40000

# Allow delve to run on Alpine based containers.
RUN apk add --no-cache libc6-compat

WORKDIR /

COPY --from=build-env /app /app
COPY --from=build-env /go/bin/dlv /

# Run delve
CMD ["/dlv", "--listen=:40000", "--headless=true", "--api-version=2", "exec", "/app"]