package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"net/url"
)

func main() {
	r := &mux.Router{}
	r.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		q, _ := url.ParseQuery(request.URL.RawQuery)
		str := fmt.Sprintf("Hello %v,", q.Get("name"))
		if _, err := writer.Write([]byte(str)); err != nil {
			return
		}
	})

	if err := http.ListenAndServe(":8080", r); err != nil {
		return
	}
}
